#!/usr/bin/env iocsh.bash

################################################################
# ***********************************************
require(chopperpdu)
# -----------------------------------------------------
# Configure Environment
# -----------------------------------------------------
# Default paths to locate database and protocol
#epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(chooperpdu_DIR)db/")
epicsEnvSet("IP_ADDRESS","172.30.235.16:502")
epicsEnvSet("PORT", "502")
epicsEnvSet("PREFIX", "P:")
epicsEnvSet("P", "LabS-EMBLA:")
epicsEnvSet("R", "Chop-PDU-01:")
epicsEnvSet("TCPPORTNAME", "Chopper")
epicsEnvSet("MBSLAVEADDR", "1")

iocshLoad("$(rittal_pdu_man2_DIR)modbusconfig.iocsh","IP_ADDRESS=$(IP_ADDRESS), TCPPORTNAME=$(TCPPORTNAME), MBSLAVEADDR=$(MBSLAVEADDR)")

## Load record instances
#dbLoadRecords("chopperPDU.db","P=$(P), R=$(R)")
#dbLoadTemplate("pdu.substitutions")
dbLoadRecords("pdu.db","P=$(P), R=$(R)")
dbLoadRecords("calc.db","P=$(P), R=$(R)")
#dbLoadTemplate("abb.substitutions")
iocinit()

